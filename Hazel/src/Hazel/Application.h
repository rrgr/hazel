#pragma once

#include "Core.h"
#include "Hazel/Events/ApplicationEvent.h"
#include "Window.h"

namespace Hazel {

    class HAZEL_API Application {
    public:
        Application();
        virtual ~Application();

        void Run();
        void OnEvent(Event& e);

    private:
        bool OnWindowClose(WindowCloseEvent& e);

        bool m_Running = true;
        std::unique_ptr<Window> m_Window;
    };

    // To be defined in a CLIENT
    Application* CreateApplication();

} // namespace Hazel
